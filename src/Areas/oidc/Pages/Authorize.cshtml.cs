using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenIddict.Abstractions;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore.Models;
using OpenIddict.Server.AspNetCore;
using OpenIddict.RazorPages.Data;
using OpenIddict.RazorPages.Helpers;

namespace OpenIddict.RazorPages.Areas.oidc.Pages {
    // This Razor Page handles the Authorization Code, Implicit, and Hybrid flows
    [Authorize]
    public class AuthorizeModel : PageModel {
        private readonly OpenIddictApplicationManager<OpenIddictApplication> _applicationManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthorizeModel(OpenIddictApplicationManager<OpenIddictApplication> applicationManager,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager) {
            _applicationManager = applicationManager;
            _signInManager = signInManager;
            _userManager = userManager;

        }


        //[BindProperty(SupportsGet = true)]
        //public string ClientId { get; set; }

        public string ApplicationName { get; set; }
        public string Scope { get; set; }

        public async Task<IActionResult> OnGetAsync() {
            // if (String.IsNullOrEmpty(ClientId)) {
            //     return BadRequest("Missing client ID.");
            // }

            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

            var application = await _applicationManager.FindByClientIdAsync(request.ClientId) ??
                throw new InvalidOperationException("Details concerning the calling application cannot be found.");

            ApplicationName = await _applicationManager.GetDisplayNameAsync(application);
            Scope = request.Scope;

            return Page();
        }

        // User clicked Accept on the consent screen
        public async Task<IActionResult> OnPostAcceptAsync() {
            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

            // Retrieve the profile of the logged in user.
            var user = await _userManager.GetUserAsync(User) ??
                throw new InvalidOperationException("The user details cannot be retrieved.");

            var principal = await _signInManager.CreateUserPrincipalAsync(user);

            //Note: in this sample, the granted scopes match the requested scopes
            // but you may want to allow the user to check or uncheck specific scopes
            // Simply restrict the list of scopes but calling SetScopes
            principal.SetScopes(request.GetScopes());
            principal.SetResources("resource_server");

            foreach (var claim in principal.Claims) {
                claim.SetDestinations(ClaimsHelpers.GetDestinations(claim, principal));
            }

            //Returning a signin result will ask OpenIddict to issue the appropriate access and/or identity tokens.
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
        }

        // User clicked Deny on the consent screen (Default behavior, user must explicitely accept to avoid this)
        public IActionResult OnPost() => Forbid(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
    }
}
