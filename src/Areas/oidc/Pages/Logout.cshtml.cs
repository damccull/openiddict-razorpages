using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenIddict.Server.AspNetCore;
using OpenIddict.RazorPages.Data;

namespace OpenIddict.RazorPages.Areas.oidc.Pages {
    //This razor page handles the Logout flows for Code and Implicit
    [Authorize]
    public class LogoutModel : PageModel {
        private readonly SignInManager<ApplicationUser> _signInManager;

        public LogoutModel(SignInManager<ApplicationUser> signInManager) {
            _signInManager = signInManager;

        }

        public IActionResult OnGet() {
            return Page();
        }

        // User clicked
        public async Task<IActionResult> OnPostAsync() {
            //Ask ASP.NET Core Identity to deelete the local and external cookies created
            //when the user agent is redirected from the external identity provider after
            //a successful authentication flow (e.g. Google or Facebook)
            await _signInManager.SignOutAsync();

            var properties = new AuthenticationProperties
            {
                RedirectUri = "/"
            };

            //Returning the SignOutResult will ask OpenIddict to redirect the user agent to the
            //post_logout_redirect_uri specified by the client application
            return SignOut(properties, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
        }
    }
}
