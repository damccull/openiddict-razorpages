using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenIddict.Abstractions;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore.Models;
using OpenIddict.Server.AspNetCore;
using OpenIddict.RazorPages.Data;
using static OpenIddict.Abstractions.OpenIddictConstants;
using OpenIddict.RazorPages.Helpers;

namespace OpenIddict.RazorPages.Areas.oidc.Pages {
    //This razor page handles the Device Flow
    [Authorize]
    public class VerifyModel : PageModel {
        private readonly OpenIddictApplicationManager<OpenIddictApplication> _applicationManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public VerifyModel(OpenIddictApplicationManager<OpenIddictApplication> applicationManager,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager) {
            _applicationManager = applicationManager;
            _signInManager = signInManager;
            _userManager = userManager;

        }

        public string ApplicationName { get; set; }
        public string Scope { get; set; }
        public string UserCode { get; set; }

        public string Error { get; set; }
        public string ErrorDescription { get; set; }

        public async Task<IActionResult> OnGetAsync() {
            var request = HttpContext.GetOpenIddictServerRequest() ??
                throw new InvalidOperationException("The OpenID Connect request cannot retrieved.");

            //If the user code was specified in the query string (e.g. as part of the verification_uri_complete),
            //render a form to ask the user to enter the user code manually (non-digit characters are automatically ignore)
            if (string.IsNullOrEmpty(request.UserCode)) {
                return Page();
            }

            //Retrieve the claims principal associated with the user code.
            var result = await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            if (result.Succeeded) {
                //Retrieve the application details from the database using the client_id stored in the principal
                var application = await _applicationManager.FindByClientIdAsync(result.Principal.GetClaim(Claims.ClientId)) ??
                    throw new InvalidOperationException("Details concerning the calling client application cannot be found.");

                ApplicationName = await _applicationManager.GetDisplayNameAsync(application);
                Scope = string.Join(" ", result.Principal.GetScopes());
                UserCode = request.UserCode;
                return Page();
            }

            //If the input code is not valid, redisplay the form
            Error = result.Properties.GetString(OpenIddictServerAspNetCoreConstants.Properties.Error);
            ErrorDescription = result.Properties.GetString(OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription);
            return Page();
        }

        // User clicked Accept on the consent screen
        public async Task<IActionResult> OnPostAcceptAsync() {
            //Get the logged in user's profile
            var user = await _userManager.GetUserAsync(User) ??
                throw new InvalidOperationException("The user details cannot be retrieved.");

            //Retrieve the claims principal associated with the user code
            var result = await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            if (result.Succeeded) {
                var principal = await _signInManager.CreateUserPrincipalAsync(user);

                // Note: in this sample, the granted scopes match the requested scope
                // but you may want to allow the user to uncheck specific scopes.
                // For that, simply restrict the list of scopes before calling SetScopes.
                principal.SetScopes(result.Principal.GetScopes());
                principal.SetResources("resource_server");

                foreach (var claim in principal.Claims) {
                    claim.SetDestinations(ClaimsHelpers.GetDestinations(claim, principal));
                }

                var properties = new AuthenticationProperties
                {
                    //This property points to the address OpenIddict will automatically
                    //redirect the user to after validating the authorization command.
                    RedirectUri = "/"
                };

                return SignIn(principal, properties, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            }

            //If the input code is not valid, redisplay the form
            Error = result.Properties.GetString(OpenIddictServerAspNetCoreConstants.Properties.Error);
            ErrorDescription = result.Properties.GetString(OpenIddictServerAspNetCoreConstants.Properties.ErrorDescription);
            return Page();

        }


        // User clicked Deny on the consent screen (Default behavior, user must explicitely accept to avoid this)
        public IActionResult OnPost() {
            var properties = new AuthenticationProperties
            {
                //This property points to the address OpenIddict will automatically
                //redirect the user to after validating the authorization command.
                RedirectUri = "/"
            };

            return Forbid(properties, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
        }
    }
}
